#include <iostream>
#include <fstream>
#include <vector>
#include <filesystem>
#include <algorithm>
#include "Pattern.h"

struct EpochVector3 {
    float x, y, z;
};
struct DynamicEntity {
    unsigned int m_nRegionIndex;
    unsigned int m_nEntityType;
    unsigned int m_nTotalCount;
    float m_dwCreateInterval;
    float m_dwCreateIntervalMax;
    unsigned int m_nGroupIndex;
    EpochVector3 m_Position;
    float m_Gen_fMinRadius;
    float m_Gen_fMaxRadius;
    float m_Act_fMinRadius;
    float m_Act_fMaxRadius;
    std::vector<EpochVector3> m_Waypoints;
};

template<typename T>
T Read(std::istream &stream) {
    T t{};
    stream.read(reinterpret_cast<char *>(&t), sizeof(T));
    return t;
}

template<typename T>
void Write(std::ostream &stream, const T &t) {
    stream.write(reinterpret_cast<const char *>(&t), sizeof(T));
}


std::vector<DynamicEntity> ReadMonsterFile(const std::filesystem::path &path) {
    std::vector<DynamicEntity> entities;

    std::ifstream stream(path, std::ifstream::binary);

    auto count = Read<unsigned int>(stream);
    for (unsigned int i = 0; i < count; ++i) {
        DynamicEntity entity{
                .m_nRegionIndex = Read<unsigned int>(stream), .m_nEntityType = Read<unsigned int>(stream),
                .m_nTotalCount =Read<unsigned int>(stream), .m_dwCreateInterval = Read<float>(stream),
                .m_dwCreateIntervalMax = Read<float>(stream), .m_nGroupIndex =Read<unsigned int>(stream),
                .m_Position = Read<EpochVector3>(stream), .m_Gen_fMinRadius = Read<float>(stream),
                .m_Gen_fMaxRadius =Read<float>(stream), .m_Act_fMinRadius =Read<float>(stream),
                .m_Act_fMaxRadius =Read<float>(stream), .m_Waypoints{}
        };
        auto waypointCount = Read<unsigned int>(stream);
        for (unsigned int w = 0; w < waypointCount; ++w) {
            entity.m_Waypoints.push_back(Read<EpochVector3>(stream));
        }
        entities.push_back(entity);
    }
    return entities;
}

struct Pattern {
    PatternData data{};
    std::vector<PatternData::ObjectGate> m_Gates;
};

Pattern ReadPatternFile(const std::filesystem::path &path) {
    Pattern pattern;

    std::ifstream stream(path, std::ifstream::binary);
    auto fSize = std::filesystem::file_size(path);

    pattern.data.m_Id = Read<PatternData::PatternID>(stream);
    pattern.data.m_PatternNumber = Read<unsigned short>(stream);
    pattern.data.m_EventValue = Read<unsigned short>(stream);
    pattern.data.m_ObjectGateCount = Read<byte>(stream);
    pattern.data.m_FieldType = Read<ePatternFieldType>(stream);

    auto gateCount = (fSize - 15) / sizeof(PatternData::ObjectGate);
    for (unsigned int i = 0; i < gateCount; ++i) {
        auto gate = Read<PatternData::ObjectGate>(stream);
        pattern.m_Gates.push_back(gate);
    }
    return pattern;
}

void SavePatternFile(const std::filesystem::path &path, const Pattern &pattern) {
    std::ofstream stream(path, std::ifstream::binary);

    Write<PatternData::PatternID>(stream, pattern.data.m_Id);
    Write<unsigned short>(stream, pattern.data.m_PatternNumber);
    Write<unsigned short>(stream, pattern.data.m_EventValue);
    Write<byte>(stream, pattern.data.m_ObjectGateCount);
    Write<ePatternFieldType>(stream, pattern.data.m_FieldType);


    for (auto &gate : pattern.m_Gates) {
        Write<PatternData::ObjectGate>(stream, gate);
    }
}

char *getCmdOption(char **begin, char **end, const std::string &option) {
    char **itr = std::find(begin, end, option);
    if (itr != end && ++itr != end) {
        return *itr;
    }
    return nullptr;
}

bool cmdOptionExists(char **begin, char **end, const std::string &option) {
    return std::find(begin, end, option) != end;
}

void
ConvertMonsterToPattern(const std::filesystem::path &inputMonsterPath, const std::filesystem::path &inputPtrPath, bool append) {
    std::vector<DynamicEntity> monsterEntities = ReadMonsterFile(inputMonsterPath);
    Pattern pattern = ReadPatternFile(inputPtrPath);

    if (!append) {
        auto it = std::find_if(pattern.m_Gates.begin(), pattern.m_Gates.end(),
                               [](auto &gate) {
                                   return gate.m_GateType == eObjectGateType::OBJECTGATE_TYPE_STARTPOINT;
                               });
        if (it != pattern.m_Gates.end()) {
            PatternData::ObjectGate teleportGate = *it;
            pattern.m_Gates.clear();
            pattern.m_Gates.push_back(teleportGate);
        }
    }

    for (auto &monsterEntity : monsterEntities) {
        float posX = roundf((monsterEntity.m_Position.x / 512.0f) * 100) / 100;
        float posZ = roundf((monsterEntity.m_Position.z / 512.0f) * 100) / 100;
        unsigned int id = pattern.m_Gates.size() + 1;
        auto interval = static_cast<unsigned int>(monsterEntity.m_dwCreateInterval) * 1000;
        PatternData::ObjectGate newGate{
                .m_Pos{posX, posZ}, .m_Id = id, .m_GateType= eObjectGateType::OBJECTGATE_TYPE_MONSTER,
                .m_IsRandom = false, .m_QuiryID = monsterEntity.m_nEntityType, .m_Degree = 0, .m_Angle = 0.0f,
                .m_LinkIndex = static_cast<unsigned short>(monsterEntity.m_nGroupIndex), .m_LiveTime = 100000000,
                .m_DelayTime = 0, .m_AttackAttribute.m_AttackType = 1, .m_IsGenTime = 1, .m_GenPopTime = interval,
                .m_MaxGenCount = 60000, .m_RewardPoint = 0, .m_RaceType = 0, .m_Patrol{}
        };
        for (std::size_t i = 0; i < monsterEntity.m_Waypoints.size() && i < 5; ++i) {
            auto &waypoint = monsterEntity.m_Waypoints[i];
            float waypointPosX = roundf((waypoint.x / 512.0f) * 100) / 100;
            float waypointPosZ = roundf((waypoint.z / 512.0f) * 100) / 100;
            newGate.m_Patrol[i] = {.m_IsSet = 1, .m_Pos{waypointPosX, waypointPosZ}};
        }
        pattern.m_Gates.push_back(newGate);
    }
    std::filesystem::path outPath = inputPtrPath;
    outPath = outPath.replace_extension("_output.ptr");
    SavePatternFile(outPath, pattern);
    std::cout << "Finished converting.\n";
}

int main(int argc, char *argv[]) {
    static_assert(sizeof(PatternData::ObjectGate) == 124, "Invalid ObjectGate sizeof value");
    if (argc < 5) {
        std::cerr
                << "No arguments provided.\nRequired -input_monster [path] -input_pattern [path]\nOptional: -append (Doesn't remove existing pattern gates).\n";
        return 1;
    }
    if (!cmdOptionExists(argv, argv + argc, "-input_monster")) {
        std::cerr << "No -input_monster [path] argument provided.\n";
        return 1;
    }
    std::filesystem::path inputPath(getCmdOption(argv, argv + argc, "-input_monster"));
    if (!std::filesystem::exists(inputPath) || !std::filesystem::is_regular_file(inputPath)) {
        std::cerr << "No such input file.\n";
        return 1;
    }
    if (!cmdOptionExists(argv, argv + argc, "-input_pattern")) {
        std::cerr << "No -input_pattern [path] argument provided.\n";
        return 1;
    }
    std::filesystem::path outputPath(getCmdOption(argv, argv + argc, "-input_pattern"));
    if (!std::filesystem::exists(outputPath) || !std::filesystem::is_regular_file(outputPath)) {
        std::cerr << "No such input file.\n";
        return 1;
    }
    bool append = false;
    if (cmdOptionExists(argv, argv + argc, "-append")) {
        append = true;
    }
    ConvertMonsterToPattern(inputPath, outputPath, append);
    return 0;
}

cmake_minimum_required(VERSION 3.17)
project(MonsterToPattern)

set(CMAKE_CXX_STANDARD 20)

add_executable(MonsterToPattern main.cpp Pattern.h)


enum ePatternSize : unsigned char {
    PATTERN_SIZE_NONE = 0x0,
    PATTERN_SIZE_SMALL = 0x8,
    PATTERN_SIZE_SnM = 0x10,
    PATTERN_SIZE_MIDDLE = 0x20,
    PATTERN_SIZE_MnB = 0x40,
    PATTERN_SIZE_BIG = 0x80,
    PATTERN_SIZE_CNT = 0x6,
};


enum ePatternStageType : unsigned char {
    PATERN_STAGETYPE_NONE = 0x0,
    PATERN_STAGETYPE_LOBBY = 0x1,
    PATERN_STAGETYPE_NORMAL = 0x2,
    PATERN_STAGETYPE_ELITE = 0x3,
    PATERN_STAGETYPE_BOSS = 0x4,
    PATERN_STAGETYPE_CNT = 0x5,
};


enum ePatternContentsType : unsigned char {

    PATTERN_CONTENTSTYPE_ELEMENT = 0x0,
    PATTERN_CONTENTSTYPE_XMAS = 0x1,
    PATTERN_CONTENTSTYPE_ESHIKINA = 0x2,
    PATTERN_CONTENTSTYPE_MAXCNT = 0x3,

};


enum ePatternEventType : unsigned char {
    PATTERN_EVENTTYPE_NONE = 0x0,
    PATTERN_EVENTTYPE_STANDARD = 0x1,
    PATTERN_EVENTTYPE_MKILL = 0x2,
    PATTERN_EVENTTYPE_TIMEATTACK = 0x3,
    PATTERN_EVENTTYPE_BONUS = 0x4,
    PATTERN_EVENTTYPE_AUTO = 0x5,
    PATTERN_EVENTTYPE_MAXCNT = 0x6,
};


enum ePatternFieldType : unsigned short {
    BEGIN_FIELD_TYPE = 0x0,
    FIELD_TYPE_NONE = 0x0,
    FIELD_TYPE_F = 0x1,
    FIELD_TYPE_W = 0x2,
    FIELD_TYPE_T = 0x3,
    FIELD_TYPE_S = 0x4,
    FIELD_TYPE_E = 0x5,
    FIELD_TYPE_SQUARE = 0x6,
    FIELD_TYPE_O = 0x7,
    END_FIELD_TYPE = 0x7,
    MAX_FIELD_TYPE = 0x8,
};


enum eObjectGateType : unsigned char {
    OBJECTGATE_TYPE_NPC = 0x0,
    OBJECTGATE_TYPE_MAGICBOX = 0x1,
    OBJECTGATE_TYPE_MONSTER = 0x2,
    OBJECTGATE_TYPE_NPCGUARD = 0x3,
    OBJECTGATE_TYPE_STARTPOINT = 0x4,
    OBJECTGATE_TYPE_SKILL = 0x5,
    OBJECTGATE_TYPE_MAX = 0x6,
};
using byte = unsigned char;

struct PatternData {
    struct PatternID {
        int vfptr;
        ePatternSize m_Size;
        ePatternStageType m_StageType;
        ePatternEventType m_EventType;
        ePatternContentsType m_ContentsType;
    } m_Id;
    unsigned short m_PatternNumber;
    unsigned short m_EventValue;
    byte m_ObjectGateCount;
    ePatternFieldType m_FieldType;

    struct ObjectGate {
        struct Pos2 {
            float x, y;
        } m_Pos;
        unsigned int m_Id;
        eObjectGateType m_GateType;
        bool m_IsRandom;
        unsigned int m_QuiryID;
        char m_Degree;
        float m_Angle;
        unsigned short m_LinkIndex;
        unsigned int m_LiveTime;
        unsigned int m_DelayTime;
        union {
            char m_AttackType;
            char m_AttributeType;
        } m_AttackAttribute;
        int m_IsGenTime;
        unsigned int m_GenPopTime;
        unsigned short m_MaxGenCount;
        unsigned int m_RewardPoint;
        char m_RaceType;
        struct MovePos2 {
            char m_IsSet;
            Pos2 m_Pos;
        } m_Patrol[5];
    };
};
